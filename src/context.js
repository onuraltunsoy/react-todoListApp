import React, { Component } from 'react'
import axios from 'axios';
import config from './config';
const TodoContext = React.createContext();
//provider ,consumer
const reducer = (state, action) => {
    switch (action.type) {
        case "DELETE_TODO":
            return {
                ...state,
                /// eskiyi ekle todos güncelle
                todos: state.todos.filter(todo => action.payload !== todo.id),
                
                selectedTododId : action.payload === state.selectedTododId ?  state.todos.length > 1 ? state.todos.filter(todo => action.payload !== todo.id)[0].id : 0   : state.selectedTododId ,

                todoItems: action.payload === state.selectedTododId  ? [] : state.todoItems

            }
        case "DELETE_TODO_ITEM":
            return {
                ...state,
                todoItems: state.todoItems.filter(todoItem => action.payload !== todoItem.id)
            }
        case "ADD_TODO":
            return {
                ...state,
                todos: [...state.todos, action.payload],
                selectedTododId : state.todos.length === 0 ? action.payload.id : state.todos[0].id
            }
        case "ADD_TODO_ITEM":
            return {
                ...state,
                todoItems: [...state.todoItems, action.payload]
            }
        case "UPDATE_TODO":
            return {
                ...state,
                todos: state.todos.map(todo => todo.id === action.payload.id ? todo = action.payload : todo)
            }
        case "LIST_TODO_ITEM":
            return {
                ...state,
                todoItems: action.payload.todoItems,
                selectedTododId: action.payload.selectedTododId
            }
        case "LOGIN_USER":
            return {
                ...state
            }

        default:
            return state;
    }
}
export class UserProvider extends Component {

    componentDidMount = async () => {
        
        const bearertoken = localStorage.getItem('token');
        const response = await axios.get(`${config.host}/api/todo/itemList/`,
            {
                headers: {
                    Authorization: 'Bearer ' + bearertoken
                }
            }
        );
        if (response.status === 200) {
            this.setState({
                todos: response.data.data,
                selectedTododId: response.data.data.length === 0 ? 0: response.data.data[0].id,
                todoItems: response.data.data.length === 0 ? []:response.data.data[0].items
            })
           
        }

    }

    state = {
        todos: [],
        todoItems: [],
        apiUrl: '',
        selectedTododId: 0
        ,
        dispatch: action => {
            /// dispatch ile providerdan veri ietesnir oda reducera yollar reducer da işlemi yapar veriyi döner
            this.setState(state => reducer(state, action)) /// gelen istek reducerla return edecek 
        }
    }
    render() {
        return (
            //// providerla valuyu dışarı açtık
            <TodoContext.Provider value={this.state}>
                {this.props.children}
            </TodoContext.Provider>
        )
    }
}
const TodoConsumer = TodoContext.Consumer;
export default TodoConsumer;

