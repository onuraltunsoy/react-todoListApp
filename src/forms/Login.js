import React, { Component } from 'react'
import TodoConsumer from '../context';
import axios from 'axios';
import config from '../config'


class Login extends Component {
    state = {
        visible: true,
        username: '',
        password: '',
        error: false

    }
    validateForm = () => {
        const { username, password } = this.state;
        if (username === '' || password === '') {
            return false;
        }
        return true;
    }
    changeVisibility = (e) => {
        this.setState({
            visible: !this.state.visible
        })
    }
    changeInput = (e) => {

        this.setState({
            [e.target.name]: e.target.value
        });
    }
    signup = ()=>{
        this.props.history.push('/signup')
                          
    }
    signIn = async (dispatch, e) => {
        e.preventDefault();
        const { username, password } = this.state;
        const signinData = {
            usernameOrEmail: username,
            password: password
        }
        if (!this.validateForm()) {
            this.setState({
                error: true
            })
            return;
        }
        const response = await axios.post(`${config.host}/api/auth/signin`, signinData)
       
    
         if (response.status === 200) {
            localStorage.setItem('username', username);
            localStorage.setItem('token',response.data.accessToken);

            dispatch({ type: "LOGIN_USER", payload: response.data });
            this.props.history.push('/')// ana sayfaya yonlendir
        }else  {
            this.setState({
                error: true
            })
            return;
        }
        
    }
    
    render() {
        const { username, password, error } = this.state;
        return <TodoConsumer>
            {value => {
                const { dispatch } = value;
                return (
                    <div className="col-md-8 mb-4">

                        <div className="card">
                            <div className="card-header">
                                <h4>Login Form</h4>
                            </div>

                            <div className="card-body">
                                {
                                    error ?
                                        <div className='alert alert-danger'>
                                            Lütfen bilgilerinizi kontrol edin.
                                        </div>
                                        : null
                                }
                                <form onSubmit={this.signIn.bind(this, dispatch)}>
                                    <div className="form-group">
                                        <label htmlFor="name">UserName</label>
                                        <input type="text" name="username" id="id"
                                            className="form-control" placeholder="Enter User Name "
                                            value={username}
                                            onChange={this.changeInput}
                                        />

                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="name">Password</label>
                                        <input type="password" name="password" id="password"
                                            className="form-control" placeholder="Enter Password"
                                            value={password}
                                            onChange={this.changeInput}
                                        />
                                    </div>
                                   

                                    <button className="btn btn-danger btn-block center " type="submit">Login</button>
                                    <br/>
                                    <button type="button" onClick={this.signup} className="btn btn-dark btn-block mb-2">SignUp</button>

                                </form>

                            </div>
                        </div>
                    </div>
                )
            }}
        </TodoConsumer>

    }
}
export default Login;







