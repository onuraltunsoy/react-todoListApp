import React, { Component } from 'react'

export default class Logout extends Component {
    render() {
        localStorage.setItem('token','');
        this.props.history.push('/login')

        return (
            <div>
               
            </div>
        )
    }
}
