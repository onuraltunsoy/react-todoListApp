import React, { Component } from 'react'
import TodoConsumer from '../context';
import axios from 'axios';
import config from '../config'

class Login extends Component {
    state = {
        visible: true,
        name: '',
        username: '',
        email: '',
        password: '',
        errorMessge: '',
        errorData: []

    }
    validateForm = () => {
        const { name, username, email, password } = this.state;
        if (name === '' || username === '' || password === '' || email === '') {
            return false;
        }
        return true;
    }
    changeVisibility = (e) => {
        this.setState({
            visible: !this.state.visible
        })
    }
    changeInput = (e) => {

        this.setState({
            [e.target.name]: e.target.value
        });
    }
    signUp = async (dispatch, e) => {
        e.preventDefault();
        const { name, username, email, password } = this.state;
        const signUpData = {
            name,
            username,
            email,
            password,
        }
        if (!this.validateForm()) {
            this.setState({
                errorMessge: 'Litfen alanları kontrol edniniz'
            })
            return;
        }
        axios.post(`${config.host}/api/auth/signup`, signUpData)
            .then(response => {
                if (response.data.status !== 200) {
                    console.log('res200000', response);
                }
            })
            .catch(error => {
                console.log('error.response', error.response)
                if (error.response.data != null) {
                    console.log('error.response.data', error.response.data);
                    this.setState({
                        errorMessge: error.response.data.message,
                        errorData: error.response.data.data
                    })
                } else {
                    console.log('errorssaasf', error)
                }
            });
    }
    
    render() {
        const { name, username, password, email, errorMessge, errorData } = this.state;
        return <TodoConsumer>
            {value => {
                const { dispatch } = value;
                return (
                    <div className="col-md-8 mb-4">

                        <div className="card">
                            <div className="card-header">
                                <h4>SignUp Form</h4>
                            </div>

                            <div className="card-body">
                                { errorMessge ?<div className='alert alert-danger '>{errorMessge}</div>: null }
                                {
                                    errorData.map(err => {
                                    return (
                                        <div className='alert alert-danger '>{err.fieldName} : {err.message} </div>
                                    )
                                })
                                }
                                <form onSubmit={this.signUp.bind(this, dispatch)}>
                                    <div className="form-group">
                                        <label htmlFor="name">Name</label>
                                        <input type="text" name="name" id="name"
                                            className="form-control" placeholder="Enter Name "
                                            value={name}
                                            onChange={this.changeInput}
                                        />

                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="name">User Name</label>
                                        <input type="text" name="username" id="username"
                                            className="form-control" placeholder="Enter User Name "
                                            value={username}
                                            onChange={this.changeInput}
                                        />

                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="email">Email</label>
                                        <input type="text" name="email" id="email"
                                            className="form-control" placeholder="Enter email"
                                            value={email}
                                            onChange={this.changeInput}
                                        />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="name">Password </label> 
                                        <input type="password" name="password" id="password"
                                            className="form-control" placeholder="Enter Password"
                                            value={password}
                                            onChange={this.changeInput}
                                        />
                                    </div>
                                   <button className="btn btn-danger btn-block " type="submit">SignUp</button>
                                </form>

                            </div>
                        </div>
                    </div>
                )
            }}
        </TodoConsumer>

    }
}
export default Login;







