import React, { Component } from 'react'
/////import posed from 'react-pose';
import TodoConsumer from '../context';
import axios from 'axios';
import config from '../config'

class AddTodo extends Component {
    state = {
        visible: true,
        name: '',
        error: false

    }
    validateForm = () => {
        const { name } = this.state;
        if (name === '') {
            return false;
        }
        return true;
    }
    clearTodoState = () => {
        this.setState({

            name: ''
        })
    }

    changeInput = (e) => {

        this.setState({
            [e.target.name]: e.target.value
        });
    }
    addTodo = async (dispatch, e) => {
        e.preventDefault();
        const { name } = this.state;
        const newTodo = {
            name: name
        }
        if (!this.validateForm()) {
            this.setState({
                error: true
            })
            return;
        }else{
            this.setState({
                error: false
            })
        }
        const bearertoken = localStorage.getItem('token');
        const response = await axios.post(`${config.host}/api/todo/itemList/`, newTodo,
            {
                headers: {
                    Authorization: 'Bearer ' + bearertoken
                }
            }
        )
        if (response.status === 200) {
            dispatch({ type: "ADD_TODO", payload: response.data });
            this.clearTodoState();
        }
       //// this.props.history.push('/')// ana sayfaya yonlendir
    }

    render() {
        const { name, error } = this.state;
        return <TodoConsumer>
            {value => {
                const { dispatch } = value;
                return (
                    <div className="col-md-12 mt-2">
                        <div className="card">
                            <div className="card-body">
                                {
                                    error ?
                                        <div className='alert alert-danger'>
                                            Lütfen bilgilerinizi kontrol edin.
                                        </div>
                                        : null
                                }
                                <form onSubmit={this.addTodo.bind(this, dispatch)}>
                                    <div className="form-group">
                                        <input type="text" name="name" id="id"
                                            className="form-control" placeholder="Enter Name"
                                            value={name}
                                            onChange={this.changeInput}
                                        />

                                    </div>
                                    <button className="btn btn-danger btn-block " type="submit">Add Todo</button>
                                </form>

                            </div>
                        </div>
                    </div>
                )
            }}
        </TodoConsumer>

    }
}
export default AddTodo;
