import React, { Component } from 'react';
import './App.css';
import Navbar from "./layout/Navbar";
import Todos from './components/Todos';
import AddTodo from './forms/AddTodo';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import NotFound from './pages/NotFound';
import Login from './forms/Login';
import Logout from './forms/Logout';
import Signup from './forms/Signup';




class App extends Component {

  render() {
    return (
      <Router>
        <div  className = 'container'> 
          <Navbar title="TodoLis App" />
          <hr />
          <Switch>
            <Route exact path='/' component={Todos} />
            <Route exact path='/add' component={AddTodo} />
            <Route exact path='/login' component={Login} />
            <Route exact path='/logout' component={Logout} />
            <Route exact path='/signup' component={Signup} />

            <Route component={NotFound} />
          </Switch>


        </div>
      </Router>

    );
  }
}


export default App;
