import React, { Component } from 'react'
import Todo from './Todo';
import TodoConsumer from '../context';
import AddTodo from '../forms/AddTodo';
import TodoItems from './TodoItems';


class Todos extends Component {
    render() {
        let tokenState = false;
        if (localStorage.getItem('token')) {
            tokenState = true;
        }
        if (!tokenState) {
            this.props.history.push('/login')

        }
        return (

            <TodoConsumer>
                {
                    value => {
                        const { todos } = value;
                        return (


                            <div className="container">

                                <div className="row">
                                    <div className="col-md-4">
                                        {
                                            todos.map(todo => {
                                                return (
                                                   
                                                    <Todo
                                                        key={todo.id}
                                                        id={todo.id}
                                                        name={todo.name}
                                                    />
                                                )
                                            })
                                        }
                                        <AddTodo></AddTodo>

                                    </div>
                                    <div className="col-md-8">

                                        <TodoItems todoItems={value.todoItems}></TodoItems>
                                    </div>
                                </div>
                            </div>
                        )
                    }
                }
            </TodoConsumer>
        )


    }
}

export default Todos;
