import React, { Component } from 'react'
import TodoConsumer from '../context';
import axios from 'axios';
import config from '../config';



class AddTodoItem extends Component {
    state = {
        visible: true,
        name: '',
        descripiton : '',
        deadline :'2019-07-27',
        error: false

    }
    validateForm = () => {
        const { name } = this.state;
        if (name === '') {
            return false;
        }
        return true;
    }
    clearTodoState = () => {
        this.setState({

            name: '',
            descripiton : ''

        })
    }

    changeInput = (e) => {

        this.setState({
            [e.target.name]: e.target.value
        });
    }
    addTodo = async (dispatch, e) => {
        e.preventDefault();
        const { name ,descripiton,deadline } = this.state;
        const {todoId } = this.props;

        const newTodoItem = {
            name,
            descripiton ,
            deadline
        }
        if (!this.validateForm()) {
            this.setState({
                error: true
            })
            return; 
        }
        const bearertoken = localStorage.getItem('token');
        const response = await axios.post(`${config.host}/api/todo/item/${todoId}`, newTodoItem,
            {
                headers: {
                    Authorization: 'Bearer ' + bearertoken
                }
            }
        )
        if (response.status === 200) {
            dispatch({ type: "ADD_TODO_ITEM", payload: response.data });
            this.clearTodoState();
        }
       //// this.props.history.push('/')// ana sayfaya yonlendir
    
    }

    render() {
        const { name, descripiton ,error } = this.state;
        return <TodoConsumer>
            {value => {
                const { dispatch } = value;
                return (
                    <div className="col-md-12 mt-2">
                        <div className="card">
                            <div className="card-header" style={{height : '50px', padding: '10px 15px'}}>
                                <h4>Add Todo Item </h4>
                            </div>

                            <div className="card-body">
                                {
                                    error ?
                                        <div className='alert alert-danger'>
                                            Lütfen bilgilerinizi kontrol edin.
                                        </div>
                                        : null
                                }
                                <form onSubmit={this.addTodo.bind(this, dispatch)}>
                                    <div className="form-group">
                                        <label htmlFor="name">Name</label>
                                        <input type="text" name="name" id="id"
                                            className="form-control" placeholder="Enter Name"
                                            value={name}
                                            onChange={this.changeInput}
                                        />      
                                          <label htmlFor="name">Description</label>

                                         <input type="text" name="descripiton" id="id"
                                            className="form-control" placeholder="Enter Description"
                                            value={descripiton}
                                            onChange={this.changeInput}
                                        />
                                    
                                    </div>
                                    <button className="btn btn-danger btn-block " type="submit">Add Todo Item</button>
                                </form>

                            </div>
                        </div>
                    </div>
                )
            }}
        </TodoConsumer>

    }
}
export default AddTodoItem;
