import React, { Component } from 'react'
import PropTypes from 'prop-types'
import TodoConsumer from '../context';
import axios from 'axios';
import config from '../config';

//////import { Link } from 'react-router-dom';

class Todo extends Component {
    state = {
        isSelected: false,
        selectedTododId: 0
    }
    static defaultProps = {
        name: "bilgi yok",
    }

    onDeleteTodo = async (dispatch, e) => {
        const { id } = this.props;

        /// deleteRequest
        const bearertoken = localStorage.getItem('token');
        var response = await axios.delete(`${config.host}/api/todo/itemList/${id}`,
            {
                headers: {
                    Authorization: 'Bearer ' + bearertoken
                }
            }
        );
        if (response.status === 202) { ////202 =  accepted
            dispatch({
                type: "DELETE_TODO",
                payload: id
            });
        }

    }


    onClickeEvent = async (dispatch, e) => { // arrow funtion şeklinde yazılırsa bind etmeye gerek yok
        const { id } = this.props;
        this.setState({
            isSelected: !this.state.isSelected

        })
        
        const token = localStorage.getItem('token');
        var response = await axios.get(`${config.host}/api/todo/item/list/${id}`,
            {
                headers: {
                    Authorization: 'Bearer ' + token
                }
            }
        );
        if (response.status === 200) {
            dispatch({
                type: "LIST_TODO_ITEM",
                payload: {
                    "todoId": id,
                    "todoItems": response.data.data,
                    "selectedTododId": id
                }
            });
        }

    }


    render() {

        const { id, name } = this.props;
        return (
            <TodoConsumer>
                {
                    value => {
                        const { dispatch } = value;

                        return (
                            <div>
                             
                                 <div className="col-md-12" >
                                    <div className="list-group-item list-group-item-action" onClick={this.onClickeEvent.bind(this, dispatch)} 
                                      style={ id === value.selectedTododId ? { backgroundColor: '#3498DB ', color: 'white' } : null }
 >
                                        <div className="d-flex justify-content-between"   >

                                            <h4 className="d-inline" onClick={this.onClickeEvent.bind(this, dispatch)}>{name}</h4>
                                            <i onClick={this.onDeleteTodo.bind(this, dispatch)} className="far fa-trash-alt" style={{ cursor: "pointer" }}></i>
                                        </div>


                                    </div>
                                </div> 
                            </div>
                        )
                    }
                }
            </TodoConsumer>
        )

    }
}
Todo.propTypes = {
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
}

export default Todo;
