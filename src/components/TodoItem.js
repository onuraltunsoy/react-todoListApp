import React, { Component } from 'react'
import PropTypes from 'prop-types'
import TodoConsumer from '../context';
import axios from 'axios';
import config from '../config';


class TodoItem extends Component {
    state = {
        isVisible: false
    }
    static defaultProps = {
        name: "bilgi yok",
        description: "bilgi yok",
        deadline: "bilgi yok"
    }

    onDeleteTodoItem = async  (dispatch,e) => {
        const {id} =  this.props;
        /// deleteRequest
        const bearertoken  = localStorage.getItem('token');
        var response = await  axios.delete(`${config.host}/api/todo/item/${id}`,
        {
            headers: {
                Authorization: 'Bearer ' + bearertoken
            }
        }
        );
        if(response.status === 202){
            dispatch({
               type : "DELETE_TODO_ITEM",
               payload : id
            });
        }
        
    }
    componentWillUnmount() {
    }
    onClickeEvent = (e) => { // arrow funtion şeklinde yazılırsa bind etmeye gerek yok

        this.setState({
            isVisible: !this.state.isVisible
        })

    }
    render() {
        ///destructing
        const { name, description, deadline } = this.props;
        const { isVisible } = this.state;
        return (
            <TodoConsumer>
            {
                value => {
                    const { dispatch } = value;
                    return (
                        <div className="col-md-12 " >
                            
                            <div className="card" style = {isVisible ? {backgroundColor : '#3498DB ' ,color :'white'} : null}>
                                <div className="card-header d-flex justify-content-between " style={{height : '50px', padding: '10px 15px'}}>

                                    <h4 className="d-inline" onClick={this.onClickeEvent}>{name}</h4>
                                    <i onClick={this.onDeleteTodoItem.bind(this,dispatch)} className="far fa-trash-alt" style={{ cursor: "pointer" }}></i>
                                </div>
                                {
                                    isVisible ?
                                        <div className="card-body" >
                                            <p className="card-text"> description :{description}</p>
                                            <p className="card-text"> deadline :{deadline.split('T')[0]}</p>
                                            <p>{this.state.test}</p>
                                           {/* <Link to = {`edit/${id}`} className = 'btn btn-dark btn-block'>Update TodoItem</Link> */}
                                        </div>
                                        : null
                                }

                            </div>
                        </div>
                    )
                }
            }
            </TodoConsumer>
        )

    }
}
TodoItem.propTypes = {
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    deadline: PropTypes.instanceOf(Date).isRequired
}

export default TodoItem;
