import React, { Component } from 'react'
import TodoItem from './TodoItem';
import TodoConsumer from '../context';
import AddTodoItem from './AddTodoItem';


class TodoItems extends Component {

    render() {

        return (
            <TodoConsumer>
                {
                    value => {
                        const { todoItems, selectedTododId } = value;
                        return (
                            <div>
                                {
                                    todoItems.map(todoItem => {
                                        return (
                                            <TodoItem
                                                key={todoItem.id}
                                                id={todoItem.id}
                                                name={todoItem.name}
                                                description={todoItem.description}
                                                deadline={todoItem.deadline}
                                            />
                                        )
                                    })
                                }
                                {
                                    selectedTododId === 0 ? null :

                                        <AddTodoItem todoId={selectedTododId}></AddTodoItem>
                                     
                                }
                               
                            </div>
                        )
                    }
                }
            </TodoConsumer>
        )


    }
}

export default TodoItems;
