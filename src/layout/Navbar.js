import React from "react";
import PropTypes from "prop-types"
import { Link } from 'react-router-dom';

const Navbar = (props) => {
    const { title } = props;
    let tokenState = false;
    if(localStorage.getItem('token')){
        tokenState =true;
    }
    return (
        <div>

            <nav className="navbar-nav navbar-expand navbar-dark bg-dark mb-3 p-3">
                <a href='/' className='navbar-brand'>{title}</a>
                <ul className='navbar-nav ml-auto'>
                    <li className='nav-item active'>
                        <Link to='/' className='nav-link'>Home</Link>
                    </li>
                    {tokenState === false ?
                        <li className='nav-item active' >
                            <Link to='/login' className='nav-link'>Login</Link>
                        </li> :
                        <li className='nav-item active' >
                            <Link to='/logout' className='nav-link'>Logout</Link>
                        </li>

                    }

                </ul>
            </nav>
        </div>
    )
}
Navbar.propTypes = {
    title: PropTypes.string.isRequired
}
Navbar.defaultProps = {
    title: "Deafult app title"
}
export default Navbar;